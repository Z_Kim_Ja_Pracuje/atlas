﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Model {
    public class Protokol {
        public int Id { get; set; }
        public string Nazwa { get; set; }

        // dyskusyjne, mozna counta zrobic na bazie
        public int IloscPartiiMiesni { get; set; }

        public int IloscPowtorzen { get; set; }
    }
}
