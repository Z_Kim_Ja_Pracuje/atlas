﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Model
{
    public interface IRepository<T>
    {
        List<T> GetAll();
        T FindById(int id);
        T Find(Action<T> action);
        void Add(T entity);
        void Update(T entity);
        bool Exists(int id);
        void Remove(T entity);
    }
}
