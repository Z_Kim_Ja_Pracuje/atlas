﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Model
{
    public class PartiaMiesniRepository : IRepository<PartiaMiesni> {
        public void Add(PartiaMiesni entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PartiaMiesni>("PartiaMiesni");
                collection.Upsert(entity);
            }
        }

        public bool Exists(int id) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PartiaMiesni>("PartiaMiesni");
                return collection.Exists(x => x.Id == id);
            }
        }

        public PartiaMiesni Find(Action<PartiaMiesni> action) {
            throw new NotImplementedException();
        }

        public PartiaMiesni FindById(int id) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PartiaMiesni>("PartiaMiesni");
                return collection.FindOne(x => x.Id == id);
            }
        }

        public List<PartiaMiesni> GetAll() {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PartiaMiesni>("PartiaMiesni");
                return collection.FindAll().ToList();
            }
        }

        public void Remove(PartiaMiesni entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PartiaMiesni>("PartiaMiesni");
                collection.Delete(x =>x.Id == entity.Id);
            }
        }

        public void Update(PartiaMiesni entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PartiaMiesni>("PartiaMiesni");
                collection.Update(entity);
            }
        }
    }
}
