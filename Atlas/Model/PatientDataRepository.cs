﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Model {
    public class PatientDataRepository : IRepository<PatientData> {
        public void Add(PatientData entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PatientData>("Pacjent");
                collection.Upsert(entity);
            }
        }

        public bool Exists(int id) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PatientData>("Pacjent");
                return collection.Exists(x => x.Id == id);
            }
        }
        
        public PatientData Find(Action<PatientData> action) {
            throw new NotImplementedException();
        }

        public PatientData FindById(int id) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PatientData>("Pacjent");
                return collection.FindOne(x => x.Id == id);
            }
        }

        public List<PatientData> GetAll() {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PatientData>("Pacjent");
                return collection.FindAll().ToList();
            }
            // throw new NotImplementedException();
        }

        public void Remove(PatientData entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PatientData>("Pacjent");
                collection.Delete(x => x.Pesel == entity.Pesel);
            }
        }

        public void Update(PatientData entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<PatientData>("Pacjent");
                collection.Update(entity);
                // collection.Insert(entity);
            }
        }
    }
}
