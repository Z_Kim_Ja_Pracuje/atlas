﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Model {
    public class PartiaMiesni {
        public int Id { get; set; }
        public int ProtokolId { get; set; }
        public string Nazwa { get; set; }

        //sciezka do zdjecia - lub nazwa, ew. cokolwiek na ten moment
        public string ZdjeciePogladowe { get; set; }

        public override bool Equals(object obj) {
            PartiaMiesni pm = obj as PartiaMiesni;
            if (pm == null)
                return false;
            return pm.Id == Id && pm.ProtokolId == ProtokolId; 
        }
    }
}
