﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Model
{
    public class ProtokolRepository : IRepository<Protokol> {
        public void Add(Protokol entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<Protokol>("Protokol");
                collection.Upsert(entity);
            }
        }

        public bool Exists(int id) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<Protokol>("Protokol");
                return collection.Exists(x => x.Id == id);
            }
        }

        public Protokol Find(Action<Protokol> action) {
            throw new NotImplementedException();
        }

        public Protokol FindById(int id) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<Protokol>("Protokol");
                return collection.FindOne(x => x.Id == id);
            }
        }

        public List<Protokol> GetAll() {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<Protokol>("Protokol");
                return collection.FindAll().ToList();
            }
        }

        public void Remove(Protokol entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<Protokol>("Protokol");
                collection.Delete(x => x.Id == entity.Id);
            }
        }

        public void Update(Protokol entity) {
            using (var db = new LiteDB.LiteDatabase("MyDb.db")) {
                var collection = db.GetCollection<Protokol>("Protokol");
                collection.Update(entity);
            }
        }
    }
}
