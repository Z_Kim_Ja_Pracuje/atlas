﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Model
{
    public class PatientData
    {
        public string NazwiskoImie => $"{Nazwisko} {Imie}";

        public int Id { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public int Wiek { get; set; }
        public DateTime DataUrodzenia { get; set; }
        public double Waga { get; set; }
        public double Wzrost { get; set; }
        public string Pesel { get; set; }
        public string Notatka { get; set; }
        public DateTime DataUtworzenia { get; set; }
        public DateTime DataModyfikacji { get; set; }
        public string Plec { get; set; }

        public PatientData() {
            DataUtworzenia = DateTime.Now;
            DataModyfikacji = DateTime.Now;
        }

        public void SignModification() {
            DataModyfikacji = DateTime.Now;
        }

    }
}
