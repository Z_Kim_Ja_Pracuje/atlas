﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Atlas.Logic;
using Atlas.Model;

namespace Atlas
{
    public static class GlobalVariables
    {
        private static Model.PatientData _patientData;
        private static Logic.Session _session;
        private static Logic.TensometerManager _tensometerManager;
        private static ChartManager _chartManager;
        

        // dopóki nie ma bazy - potem wywalić bo będzie EntityFramework
        private static ProtokolRepository protokolRepository;
        private static PartiaMiesniRepository partiaMiesniRepository;


        private static SerialPort _tensometr;
        private static Func<double> tensometerActions;


        public static PatientData PatientData {
            get => _patientData;
            set => _patientData = value;
        }

        public static Logic.Session Session {
            get => _session;
            set => _session = value;
        }

        public static SerialPort Tensometr {
            get => _tensometr;
            private set => _tensometr = value;
        }

        public static Func<double> TensometerActions {
            get => tensometerActions;
            set => tensometerActions = value;
        }

        public static TensometerManager TensometerManager {
            get => _tensometerManager;
            private set => _tensometerManager = value;
        }

        public static ProtokolRepository ProtokolRepository {
            get => protokolRepository;
            set => protokolRepository = value;
        }

        public static PartiaMiesniRepository PartiaMiesniRepository {
            get => partiaMiesniRepository;
            set => partiaMiesniRepository = value;
        }

        public static ChartManager ChartManager {
            get => _chartManager;
            set => _chartManager = value;
        }

        public static void InitChartManager(OxyPlot.Wpf.Plot chart) {
            ChartManager = new ChartManager(chart);
        }

        /// <summary>
        /// Inicjalizacja TensometerManager
        /// </summary>
        public static void InitTensometer() {
            TensometerManager = new TensometerManager();
        }

        /// <summary>
        /// Inicjalizacja sesji pomiarowej - w zasadzie wystarczy nazwa protokolu
        /// </summary>
        public static void InitSession(string protocolName) {
            Session = new Logic.Session(protocolName);
            // TensometerActions();
        }

        static GlobalVariables() {
            ProtokolRepository = new ProtokolRepository();
            PartiaMiesniRepository = new PartiaMiesniRepository();
            InitTensometer();
        }

        // public static 

    }
}
