﻿using OxyPlot.Series;
using OxyPlot.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace Atlas.Logic {
    /// <summary>
    /// Logika wykresu - dodawanie punktów, ładowanie gotowych serii danych, etc.
    /// Generalnie to zgodnie z nazwą - menadżer wykresu.
    /// 
    /// TODO: Wyświetlanie więcej niż jednej linii - w sensie porównanie pomiarów w kolejnych seriach dla tych samych grup mięśni
    ///       Modyfikacje pod wczytywanie sesji pomiarowej z bazy
    ///       I chyba tyle, może coś jeszcze wyjdzie w praniu
    /// </summary>
    public class ChartManager {
        private Plot _chart;
        private OxyPlot.Series.LineSeries _mainLineSerie;

        private DateTime startTime = DateTime.MinValue;
        private Timer refreshChart;

        public Plot Chart {
            get => _chart;
            set => _chart = value;
        }

        public OxyPlot.Series.LineSeries MainLineSerie {
            get => _mainLineSerie;
            protected set => _mainLineSerie = value;
        }

        public ChartManager() { }
        public ChartManager(Plot plot) {
            Chart = plot;
            MainLineSerie = Chart.ActualModel.Series.FirstOrDefault() as OxyPlot.Series.LineSeries;

            refreshChart = new Timer();
            refreshChart.Interval = 100;
            refreshChart.Elapsed += RefreshChart_Elapsed;
        }

        public void Restart() {
            if(MainLineSerie != null) {
                MainLineSerie.Points.Clear();
                Chart.InvalidatePlot(true);
            }
            startTime = DateTime.MinValue;
        }

        public void Init() {
            refreshChart.AutoReset = true;
            refreshChart.Start();
        }

        private void RefreshChart_Elapsed(object sender, ElapsedEventArgs e) {
            Chart.Dispatcher.Invoke(() => {
                Chart.InvalidatePlot(true);
                Chart.ActualModel.InvalidatePlot(true);
                
            });
        }

        public void StopRefreshing() {
            if(refreshChart != null) {
                refreshChart.Stop();
                refreshChart.AutoReset = false;
            }
        }


        public void AddPointToMainLine(double val) {
            if (!Chart.ActualModel.Series.Any()) {
                Chart.ActualModel.Series.Add(new OxyPlot.Series.LineSeries());
                MainLineSerie = Chart.ActualModel.Series.FirstOrDefault() as OxyPlot.Series.LineSeries;
            }

            if(startTime == DateTime.MinValue) {
                startTime = DateTime.Now;
            }
            double time = (DateTime.Now - startTime).TotalSeconds;
            MainLineSerie.Points.Add(new OxyPlot.DataPoint(time, val));
        }

        //public void AddPointToMainLine(Point point) {
        //    if (Chart.ActualModel.Series.Any()) {
        //         (Chart.ActualModel.Series[0] as OxyPlot.Series.LineSeries)
        //    }
            
        //}


    }
}
