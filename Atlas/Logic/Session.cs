﻿using Atlas.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Atlas.Logic
{
    /// <summary>
    /// Reprezentacja pojedyczej sesji pomiarowej - MUSI MIEC ODWZOROWANIE W BAZIE!!!
    /// </summary>
    public class Session
    {
        private bool _completed;
        private int _seriesNum;
        private int _actSeriesNum;
        private bool _interupted;
        private Protokol _protokol;
        private List<PartiaMiesni> _partieMiesni;
        private PartiaMiesni _actPartiaMiesni;
        private List<PartiaMiesni> _examinedPartiaMiesni;
        private List<PartiaMiesni> _toExaminePartiaMiesni;
        private PatientData _patientData;
        private Dictionary<PartiaMiesni, List<Point>>[] _dataLists;

        // zmienne aktualnego pomiaru
        private DateTime startTime = DateTime.MinValue;

        // ukończone serie
        // czego dotyczy sesja
        // hm, zrobić to na zasadzie abstracta i kolejny protokol - kolejna klasa? czy moze cos na zasadzie generycznosci?
        // slownik - partia miesni-zdjecie
        // syntezator mowy - albo pliki audio
        // lista serii pomiarowych - trzeba jeszcze klase serie zrobic


        public Session() {
            ExaminedPartiaMiesni = new List<PartiaMiesni>();
            ToExaminePartiaMiesni = new List<PartiaMiesni>();
            ActSeriesNum = 1;
        }
        public Session(Protokol protokol) : this() {
            Protokol = protokol;
            SeriesNum = Protokol.IloscPowtorzen;

            SetPartieMiesni();
        }

        public Session(string protokolName) {
            SetProtokolByNazwa(protokolName);
            SetPartieMiesni();
        }

        /// <summary>
        /// Czy sesja pomiarowa jest zakończona
        /// </summary>
        public bool Completed {
            get => _completed;
            protected set => _completed = value;
        }

        /// <summary>
        /// Ilość serii pomiarowych dla parti mięśni w protokole - w sumie to można wyciągać z protokołu
        /// </summary>
        public int SeriesNum {
            get => _seriesNum;
            protected set => _seriesNum = value;
        }

        /// <summary>
        /// Numer aktualnej serii pomiarowej - numeracja od 1
        /// </summary>
        public int ActSeriesNum {
            get => _actSeriesNum;
            protected set => _actSeriesNum = value;
        }

        /// <summary>
        /// Czy sesja pomiarowa została zakończona przed właściwym końcem - czyli czy poleciał wyjątek albo użytkownik
        /// ręcznie zakończył
        /// </summary>
        public bool Interupted {
            get => _interupted;
            protected set => _interupted = value;
        }

        /// <summary>
        /// Protokół pomiarowy do którego odnosi się sesja
        /// </summary>
        public Protokol Protokol {
            get => _protokol;
            protected set => _protokol = value;
        }

        /// <summary>
        /// Jakie partie mięśni mają być przebadane w ramach jednej serii pomiarowej
        /// </summary>
        public List<PartiaMiesni> PartieMiesni {
            get => _partieMiesni;
            protected set => _partieMiesni = value;
        }

        /// <summary>
        /// Aktualnie badana partia mięśni
        /// </summary>
        public PartiaMiesni ActPartiaMiesni {
            get => _actPartiaMiesni;
            set => _actPartiaMiesni = value;
        }

        /// <summary>
        /// Partie mięśni przebadane w ramach aktualnej serii pomiarowej 
        /// </summary>
        public List<PartiaMiesni> ExaminedPartiaMiesni {
            get => _examinedPartiaMiesni;
            protected set => _examinedPartiaMiesni = value;
        }

        public List<PartiaMiesni> ToExaminePartiaMiesni {
            get => _toExaminePartiaMiesni;
            protected set => _toExaminePartiaMiesni = value;
        }

        public PatientData PatientData {
            get => _patientData;
            set => _patientData = value;
        }

        public Dictionary<PartiaMiesni, List<Point>>[] DataLists {
            get => _dataLists;
            set => _dataLists = value;
        }


        #region Operacje na bazie
        /// <summary>
        /// Ustawienie propertasa Serie na podstawie danych w bazie i protokołu
        /// </summary>
        private void SetPartieMiesni() {
            if(Protokol != null) {
                // zaladowanie z bazy po Id protokolu
                // + ustawienie ToExaminePartiaMiesni
                PartieMiesni = GlobalVariables.PartiaMiesniRepository.GetAll(); // tymczasowo, dopóki 1 protokół
                ToExaminePartiaMiesni = GlobalVariables.PartiaMiesniRepository.GetAll();

                DataLists = new Dictionary<PartiaMiesni, List<Point>>[SeriesNum];
                for(int i = 0; i < SeriesNum; i++) {
                    DataLists[i] = new Dictionary<PartiaMiesni, List<Point>>();
                    foreach (PartiaMiesni pm in PartieMiesni) {
                        DataLists[i].Add(pm, new List<Point>());
                    }
                }
            }
        }

        /// <summary>
        /// Wyciągnięcie z bazy i ustawienie protokołu bazując na jego nazwie
        /// </summary>
        /// <param name="nazwa"></param>
        private void SetProtokolByNazwa(string nazwa) {
            if (!string.IsNullOrEmpty(nazwa)) {
                // zaladowanie protokolu z bazy po nazwie
            }
        }
        #endregion

        public void InsertPoint(double val) {
            if(startTime == DateTime.MinValue) {
                startTime = DateTime.Now;
            }

            double actTime = (DateTime.Now - startTime).TotalSeconds;

            // trzeba jeszcze narysować na wykresie
            // i przekonwertować value na kg
            val /= 100.0;
            DataLists[ActSeriesNum - 1][ActPartiaMiesni].Add(new Point(actTime, val));
            
        }


        /// <summary>
        /// Ukończenie pomiaru dla partii mięśni
        /// </summary>
        /// <returns></returns>
        public bool CompletePartiaMiesni() {
            ExaminedPartiaMiesni.Add(ActPartiaMiesni);

            var toRemove = ToExaminePartiaMiesni.FirstOrDefault(x => x.Id == ActPartiaMiesni.Id && x.ProtokolId == ActPartiaMiesni.ProtokolId);
            ToExaminePartiaMiesni.Remove(toRemove);

            ActPartiaMiesni = null;

            startTime = DateTime.MinValue;

            if (!ToExaminePartiaMiesni.Any()) {
                CompleteSeriaPomiarowa();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Ukończenie serii pomiarów
        /// </summary>
        public void CompleteSeriaPomiarowa() {
            ExaminedPartiaMiesni.Clear();
            ToExaminePartiaMiesni.AddRange(PartieMiesni);
            ActSeriesNum++;
            if(ActSeriesNum > SeriesNum) {
                CompleteSesjaPomiarowa();
            }
        }

        /// <summary>
        /// Zakończenie sesji pomiarowej
        /// </summary>
        /// <param name="interrupted"></param>
        public void CompleteSesjaPomiarowa(bool interrupted = false) {
            if (interrupted) {
                Interupted = true;
            }
            Completed = true;
            // jakieś dalsze akcje - zapis w bazie, etc.
        }

    }
}
