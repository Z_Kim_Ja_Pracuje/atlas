﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Atlas.Logic {
    /// <summary>
    /// Reprezentacja źródła danych - czy to pliki z danymi, czy baza danych, czy tensometr
    /// </summary>
    public abstract class DataSource {
        

        public DataSource() { }

        public abstract Point GetNextPoint();

        public abstract void Init();

        // public abstract void Init();

    }

    public class DataSourceProperties {
        public enum DataSourceType : byte {
            Database = 1,
            Files = 2,
            Tensometer = 4
        }

        public DataSourceType SourceType { get; set; }
        public Dictionary<string, object> Properties { get; set; }

        #region Consts
        /// <summary>
        /// Wartość w słowniku dotyczy nazwy portu - rzutuj object na string
        /// </summary>
        public static string COM => "COM";

        /// <summary>
        /// Wartość dotyczy nazwy bazy danych - rzutuj na string
        /// </summary>
        public static string DatabaseName => "DatabaseName"; 


        #endregion
    }


}
