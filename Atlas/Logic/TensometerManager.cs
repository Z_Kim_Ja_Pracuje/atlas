﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Atlas.Logic {
    /// <summary>
    /// Klasa 'zarządzająca' tensometrem
    /// </summary>
    public class TensometerManager {
        private static readonly ILog _log = LogManager.GetLogger("");

        private object _locker = new object();
        private SerialPort _tensometer;
        private Action<double> _tensometerActions;
        private Thread _receiver;
        private int _interval;
        private bool _stopReceiver = false;
        private System.Timers.Timer _timerIsOpen;
        private Action<bool> _isOpenActions;
 
        /// <summary>
        /// COM tensometru
        /// </summary>
        public SerialPort Tensometer {
            get => _tensometer;
            set => _tensometer = value;
        }

        /// <summary>
        /// Delegat przechowujący metody, które odpalają się po odczytaniu z tensometru siły
        /// Trzeba potestować - czy wystarczy double czy trzeba Point - trzeba sprawdzić o ile będzie rozjeżdżał się czas
        /// </summary>
        public Action<double> TensometerActions {
            get => _tensometerActions;
            set => _tensometerActions = value;
        }

        /// <summary>
        /// Czy tensometr działa - czy można odbierać dane
        /// </summary>
        public bool IsOpen => Tensometer?.IsOpen ?? false;

        /// <summary>
        /// Wątek odbierający dane z tensometru
        /// </summary>
        public Thread Receiver {
            get => _receiver;
            private set => _receiver = value;
        }

        /// <summary>
        /// Czas pomiędzy kolejnymi pomiarami, domyślnie 50ms
        /// </summary>
        public int Interval {
            get => _interval;
            set {
                if(value >= 10 && value <= 5000) {
                    _interval = value;
                }
            }
        }

        /// <summary>
        /// Timer sprawdzający, czy tensometr działa, czy IsOpen jest true
        /// </summary>
        public System.Timers.Timer TimerIsOpen {
            get => _timerIsOpen;
            set => _timerIsOpen = value;
        }

        /// <summary>
        /// Delegat dla metod sprawdzających czy tensometr działa
        /// </summary>
        public Action<bool> IsOpenActions {
            get => _isOpenActions;
            set => _isOpenActions = value;
        }

        /// <summary>
        /// Domyślny konstruktor - wykonywany na starcie aplikacji
        /// </summary>
        public TensometerManager() {
            Interval = 50;
            bool done = InitSerialPort();
            if (done) {
                _log.Info("Uruchomienie wątku odbierającego dane z tensometru.");
                StartReceiver();
            }
            else {
                _log.Error("Nie udało się nawiązać połączenia z tensometrem.");
            }
        }

        /// <summary>
        /// Inicjalizacja tensometru domyślnymi wartościami
        /// </summary>
        private bool InitSerialPort() {
            Tensometer = new SerialPort() {
                PortName = "COM3",
                BaudRate = 115200,
                Parity = Parity.None,
                DataBits = 8,
                Handshake = Handshake.None,
                RtsEnable = false,
                StopBits = StopBits.One
            };

            try {
                Tensometer.Open();
                return true;
            }
            catch(Exception ex) {
                _log.Error(ex);
                
                // komunikat, że nie jest podpięty czujnik
            }

            return false;
        }


        public void StartReceiver() {
            try {
                if (!IsOpen) Tensometer.Open();
                Receiver = new Thread(ReceiveFromTensometer);
                Receiver.Priority = ThreadPriority.AboveNormal;
                Receiver.Start();
            }
            catch(Exception ex) {
                _log.Error(ex);
            }
        }

        private void ReceiveFromTensometer() {
            try {
                // czas w tym miejscu jest raczej potrzebny - żeby było wiadomo, kiedy ponowić komunikat
                int startTime = 0;
                double actTime = 0;
                DateTime start = DateTime.Now;
                DateTime lastReset = DateTime.Now;
                DateTime lastPomiar = DateTime.Now;

                // ustawienie interwału
                // Tensometer.Write($"f{time}{(char)(13)}");
                ContinueReceiving();
                Stopwatch stopwatch = new Stopwatch();
                while (!_stopReceiver) {

                    try {
                        char c = (char)Tensometer.ReadChar();
                        stopwatch.Restart();
                        List<char> chars = new List<char>();

                        if (c == '*') {
                            do {
                                c = (char)Tensometer.ReadChar();
                                chars.Add(c);
                            }
                            while (c != 13);
                        }
                        else if(c == 's') {
                            c = (char)Tensometer.ReadChar();
                            if(c == '0') {
                                // odczytywanie przerwane na żądanie - w sumie to co teraz?
                            }
                            else if(c == 'E') {
                                // wygasło - trzeba ponowić
                                ContinueReceiving();
                            }
                        }
                        else {
                            continue;
                        }

                        actTime += (DateTime.Now - lastPomiar).TotalMilliseconds;
                        if (actTime + 2 * Interval > 20000) {
                            ContinueReceiving();
                        }

                        // wywołanie metod
                        string s = new string(chars.ToArray(), 0, chars.Count - 1);
                        TensometerActions(double.Parse(s));
                        stopwatch.Stop();
                        int elapsedMilis = (int)stopwatch.ElapsedMilliseconds;

                        // sleep, żeby a) nie zarżnąć procesora b) żeby odblokować na chwilę SerialPort
                        if (elapsedMilis < Interval - 1) {
                            Thread.Sleep(Interval - 1 - elapsedMilis);
                        }

                        // string s = new string(chars.ToArray(), 0, chars.Count - 1); // nowy string bez ostatniego znaku
                        // Console.WriteLine(s);
                    }
                    catch(InvalidOperationException ioex) {
                        // port nie jest otwarty - rethrow wyjątku
                        throw;
                    }
                    catch(Exception ex) {
                        _log.Error(ex);
                        // pytanie co tu jeszcze może polecieć?
                    }


                }
            }
            catch (Exception ex) {
                _log.Error(ex);
                // throw;
            }
        }

        /// <summary>
        /// Zatrzymanie wątku odbierającego dane z tensometru 
        /// </summary>
        public async void StopReceiving() {
            await Task.Run(() => {
                _stopReceiver = true;
                Task.Delay(200);

                do {
                    try {
                        Receiver.Abort();
                        Task.Delay(50);
                    }
                    catch (Exception ex) {
                        _log.Error(ex);
                    }
                }
                while (Receiver.IsAlive);

                // zatrzymanie wysyłania danych przez tensometr
                ContinueReceiving(0);
            });
        }

        /// <summary>
        /// Zmiana interwału pomiędzy kolejnymi pomiarami z tensometru
        /// </summary>
        /// <param name="milis"></param>
        public async void ChangeInterval(int milis) {
            int oldVal = Interval;
            Interval = milis;
            if (oldVal == Interval) {
                return;
            }
            else {
                await Task.Run(() => {
                    Tensometer.Write($"f{milis}{(char)13}");
                });
            }
        }

        /// <summary>
        /// Podtrzymanie pomiarów z tensometru
        /// </summary>
        /// <param name="milis">Ilość milisekund, przez które czujnik ma wysyłać dane</param>
        public void ContinueReceiving(int milis = 20000) {
            Tensometer.Write($"s{milis}{(char)13}");
        }



    }
}
