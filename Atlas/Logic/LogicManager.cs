﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Atlas.Logic
{
    /// <summary>
    /// Logika, czyli odbieranie danych, przekazywanie danych do wyświetlenia
    /// Zarządzanie przekazywaniem danych do obliczeń, rozpoczynanie/kończenie sesji pomiarowej etc.
    /// </summary>
    public class LogicManager
    {

        private static readonly ILog _log = LogManager.GetLogger("");

        private LogicManager() { }

        private static LogicManager _instance;
        public static LogicManager Instance {
            get {
                if(_instance == null) {
                    _instance = new LogicManager();
                }
                return _instance;
            }
        }

        public void ResetInstance() {
            _instance = null;
        }

        // kod pisany w tym miejscu jest mocno na pałę - przerobić to później


        private System.Threading.Thread receiver;
        private SerialPort serialPort;

        public void InitSerialPort() {
            serialPort = new SerialPort();
            serialPort.PortName = "COM3";
            serialPort.BaudRate = 115200;
            serialPort.Parity = Parity.None;
            serialPort.DataBits = 8;
            serialPort.Handshake = Handshake.None;
            serialPort.RtsEnable = false;
            serialPort.StopBits = StopBits.One;
        }

        public void InitReceive(int time = 100) {
            serialPort.Open();
            receiver = new System.Threading.Thread(() =>{
                try {
                    int startTime = 0;
                    double actTime = 0;
                    DateTime start = DateTime.Now;
                    DateTime lastReset = DateTime.Now;
                    DateTime lastPomiar = DateTime.Now;

                    // ustawienie interwału
                    serialPort.Write($"f{time}{(char)(13)}");
                    serialPort.Write($"s20000{(char)(13)}");

                    while (!EndReceive) {

                        char c = (char)serialPort.ReadChar();
                        List<char> chars = new List<char>();

                        if (c == '*') {
                            do {
                                c = (char)serialPort.ReadChar();
                                chars.Add(c);
                            }
                            while (c != 13);
                        }
                        else {
                            continue;
                        }

                        actTime += (DateTime.Now - lastPomiar).TotalMilliseconds;
                        if (actTime + 2 * time > 20000) {
                            serialPort.Write($"s20000{(char)(13)}");
                        }

                        string s = new string(chars.ToArray(), 0, chars.Count - 1); // nowy string bez ostatniego znaku
                        Console.WriteLine(s);
                        



                        //byte s = (byte)serialPort.ReadByte();
                        //List<byte> list = new List<byte>();

                        //if((char)s == '*') {
                        //    do {
                        //        s = (byte)serialPort.ReadByte();
                        //        list.Add()
                        //    }
                        //}


                    }
                }
                catch (Exception ex) {
                    _log.Error(ex);
                    // throw;
                }

            });
            
            receiver.Start();
        }

        public bool EndReceive { get; set; }

        public void AbortReceiver() {
            try {
                do {
                    receiver.Abort();
                    Thread.Sleep(200);
                }
                while (receiver.IsAlive);
            }
            catch(Exception ex) {
                _log.Error(ex);
                throw;
            }
        }



    }
}
