﻿using Atlas.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Atlas {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        // 1. Gui i odczyt dotychczasowych wyników - z .ini
        // 1.1 
        // 1.2 
        // 2. Model - dostęp do bazy, na razie zrobić połączenie z litedb, potem zmienić powiązanie na mysql
        // 3. Czyszczenie danych przy nowym badaniu - potem

        private static readonly ILog _log = LogManager.GetLogger("");

        private readonly string databaseNameKey = "DatabaseName";
        private readonly string databaseName;
        private PatientDataRepository repo = new PatientDataRepository();
        

        List<object> l = new List<object>();

        public MainWindow() {
            InitializeComponent();
            //this.Resources["PatientData"] = new PatientData() {
            //    Name = "Jan", Surname = "Kowalski", Pesel = "1221"
            //};

            // only once

            //ProtokolRepository pRep = new ProtokolRepository();
            //pRep.Add(new Protokol() {
            //    Id = 1,
            //    IloscPartiiMiesni = 6,
            //    IloscPowtorzen = 4,
            //    Nazwa = "Prostowniki/Zginacze"
            //});

            //PartiaMiesniRepository pmRep = new PartiaMiesniRepository();
            //pmRep.Add(new PartiaMiesni() {
            //    Id = 1,
            //    ProtokolId = 1,
            //    Nazwa = "Zginacze - lewa noga",
            //    ZdjeciePogladowe = ""
            //});

            //pmRep.Add(new PartiaMiesni() {
            //    Id = 2,
            //    ProtokolId = 1,
            //    Nazwa = "Zginacze - prawa noga",
            //    ZdjeciePogladowe = ""
            //});

            //pmRep.Add(new PartiaMiesni() {
            //    Id = 3,
            //    ProtokolId = 1,
            //    Nazwa = "Zginacze - obie nogi",
            //    ZdjeciePogladowe = ""
            //});

            //pmRep.Add(new PartiaMiesni() {
            //    Id = 4,
            //    ProtokolId = 1,
            //    Nazwa = "Prostowniki - lewa noga",
            //    ZdjeciePogladowe = ""
            //});

            //pmRep.Add(new PartiaMiesni() {
            //    Id = 5,
            //    ProtokolId = 1,
            //    Nazwa = "Prostowniki - prawa noga",
            //    ZdjeciePogladowe = ""
            //});

            //pmRep.Add(new PartiaMiesni() {
            //    Id = 6,
            //    ProtokolId = 1,
            //    Nazwa = "Prostowniki - obie nogi",
            //    ZdjeciePogladowe = ""
            //});



            // -only once

            GlobalVariables.InitTensometer();
            GlobalVariables.InitChartManager(PlotWykres);

            databaseName = ConfigurationManager.AppSettings[databaseNameKey];
            

            // test - uzupełnianie listy pomiarów
            List<object> list = new List<object>();
            for(int i = 0; i < 100; i++) {
                list.Add(new {
                    DataGodzina = DateTime.Now,
                    Protokol = "test",
                    Waga = "test",
                    Wzrost = "test"
                });
            }

            ListViewPomiary.ItemsSource = list;

            // test - uzupełnianie wartości w zakładce obliczenia
            var list2 = repo.GetAll();
            ListViewPacjenci.ItemsSource = list2;

            Dictionary<string, int> dict = new Dictionary<string, int>();
            
            for(int i = 0; i < 10; i++) {
                l.Add(new { Zmienna = $"Wartość{i}", Wartość = i });
                dict.Add($"Wartość{i}", i);
            }
            DataGridObliczeniaDane.ItemsSource = l;
            DataGridObliczeniaWyniki.ItemsSource = l;

        }

        #region Listenery

        private void TextBoxWyszukaj_TextChanged(object sender, TextChangedEventArgs e) {
            // wyszukiwanie - albo cache wartości albo wczytywanie za każdym razem z bazy

            string search = (sender as TextBox).Text;
            RefreshPatientList(search);
        }

        private void ButtonWyszukajWyczysc_Click(object sender, RoutedEventArgs e) {
            ClearTextSearch();
        }

        private void ListViewPomiary_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            ButtonEksportCSV.IsEnabled = ButtonOtworzWybrany.IsEnabled = ButtonRaportZWybranego.IsEnabled = ButtonUsunWybrany.IsEnabled =
                ((sender as ListView).SelectedItem != null);
        }

        private void ButtonNowyPacjent_Click(object sender, RoutedEventArgs e) {
            Dispatcher.Invoke(ShowDanePacjentaForm, System.Windows.Threading.DispatcherPriority.DataBind);
        }

        /// <summary>
        /// Modyfikacja danych pacjenta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonModyfikuj_Click(object sender, RoutedEventArgs e) {
            ShowDanePacjentaForm();
            var selected = ListViewPacjenci.SelectedValue as PatientData;
            if(selected == null) {
                Console.WriteLine("Null - co do chuja?");
                return;
            }
            FillDanePacjentaForm(selected);
        }

        /// <summary>
        /// Usuwanie pacjenta z bazy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonUsunPacjenta_Click(object sender, RoutedEventArgs e) {
            // usunąć po peselu - powinno być najłatwiej
            // kiedy będzie baza - po Id
            // odświeżanie listy!

            var pd = ListViewPacjenci.SelectedItem as PatientData;
            if(pd!= null) {
                repo.Remove(pd);
            }

            ClearTextSearch();
            RefreshPatientList();
        }


        /// <summary>
        /// Anulowanie wprowadzania danych pacjenta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonAnulujDane_Click(object sender, RoutedEventArgs e) {
            HideDanePacjentaForm();
            ResetDanePacjentaForm();
        }

        /// <summary>
        /// Zapisanie danych pacjenta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonZapiszDanePacjenta_Click(object sender, RoutedEventArgs e) {
            CreatePacjent();
        }

        /// <summary>
        /// Zapisanie danych pacjenta i rozpoczęcie pomiaru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonZapiszDaneRozPomiar_Click(object sender, RoutedEventArgs e) {
            GlobalVariables.PatientData = CreatePacjent();

            // przejście do pomiaru - czyli to co się dzieje po kliknięciu nowe badanie
            StartNoweBadanie();
        }

        /// <summary>
        /// Utworzenie i zwrócenie danych pacjenta + wyczyszczenie i ukrycie formularza
        /// </summary>
        /// <returns></returns>
        private PatientData CreatePacjent() {
            var pd = CreatePatientData();
            if (pd != null) {
                repo.Add(pd);
            }

            HideDanePacjentaForm();
            ResetDanePacjentaForm();

            ClearTextSearch();
            RefreshPatientList();

            return pd;
        }

        /// <summary>
        /// Zmiana wybranego elementu w Combo protokołu na zakładce pomiar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxProtokol_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            // załadowanie z bazy dostępnych grup mięśni
            // kurwa, jak z automatu określić powiązanie z combo z grupami mięśni? po nazwie?
            var c = sender as ComboBox;
            var selected = c.SelectedValue;

        }

        /// <summary>
        /// Wybranie pacjenta z listy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewPacjenci_Selected(object sender, SelectionChangedEventArgs e) {

            var selected = (sender as ListView).SelectedItem as PatientData;
            ButtonNoweBadanie.IsEnabled = ButtonModyfikuj.IsEnabled = ButtonUsunPacjenta.IsEnabled = (selected != null);
            GlobalVariables.PatientData = selected;

        }

        /// <summary>
        /// Wyznaczenie daty urodzenia i wieku na podstawie poprawnego peselu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBoxPesel_TextChanged(object sender, TextChangedEventArgs e) {
            var txtBox = (sender as TextBox);

            if (!System.Windows.Controls.Validation.GetHasError(txtBox as DependencyObject)) {
                (int age, DateTime birth) = CalcBirthFromPesel(txtBox.Text);
                TextBlockDataUrForm.Text = birth.ToString("dd-MM-yyyy");
                TextBlockWiekForm.Text = age.ToString();
            }
        }

        /// <summary>
        /// Przetwanie trwającej sesji pomiarowej
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrzerwijSesjePomiarowa(object sender, RoutedEventArgs e) {
            // zakończenie, ustawienie, że interrupted i przejście do jakiejś zakładki, pytanie do której?
            // do przemyślenia na później
        }

        /// <summary>
        /// Kontynuacja sesji pomiarowej - trzeba rozróżnić, kiedy tworzyć sesję a kiedy tylko kontynuować
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPomiaryDalej_Click(object sender, RoutedEventArgs e) {
            var button = sender as Button;
            if(button.Name == "ButtonPomiaryDalej") {
                // nowa sesja - składowana w GlobalVariables
                
                GlobalVariables.Session = new Logic.Session(GlobalVariables.ProtokolRepository.FindById((int)ComboBoxWybierzProtokol.SelectedValue)) {
                    // hm, no nie do końca - co jeśli pomiar dla nowego pacjenta?
                    // może lepiej ustawiać gdzieś patientData i z tego tu skorzystać?
                    PatientData = GlobalVariables.PatientData,
                    ActPartiaMiesni = GlobalVariables.PartiaMiesniRepository.FindById((int)ComboBoxWybierzMiesnie.SelectedValue)
                    
                };
                // GlobalVariables.Session
                FillDanePacjentaPomiary(GlobalVariables.Session.PatientData);
                
            }
            else {
                // kontynuacja, zebranie danych z panelu i pajechali dalszoj
                GlobalVariables.Session.ActPartiaMiesni = GlobalVariables.PartiaMiesniRepository.FindById((int)ComboBoxWybierzMiesnie2.SelectedValue);
            }
            PrzejdzDoPomiaru();
            ButtonZatrzymajPomiar.IsEnabled = false;
            ButtonPrzerwijPomiar.IsEnabled = false;
            ButtonRozpocznijPomiar.IsEnabled = false;
        }

        /// <summary>
        /// Nowe badanie - czyli zaczynamy nową sesję pomiarową
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonNoweBadanie_Click(object sender, RoutedEventArgs e) {
            StartNoweBadanie();
        }

        /// <summary>
        /// Rozpoczęcie nowego badania - wejście na panel tutoriala
        /// </summary>
        private void StartNoweBadanie() {
            TabItemPomiar.IsEnabled = true;
            OpenNewSessionTutorial();
        }

        private void ButtonUsunWybrany_Click(object sender, RoutedEventArgs e) {

        }

        private void ButtonOtworzWybrany_Click(object sender, RoutedEventArgs e) {

        }

        private void ButtonRaportZWybranego_Click(object sender, RoutedEventArgs e) {

        }

        #endregion

        #region Metody

        /// <summary>
        /// Ukrycie formularza tworzenia/edycji pacjenta
        /// </summary>
        private void HideDanePacjentaForm() {
            GridBazaPacjentowMain.RowDefinitions[0].Height = new GridLength(0);
            RowDanePacjenta.MinHeight = 0;
            GridDanePacjenta.Visibility = Visibility.Collapsed;
            GridListaPacjentow.Visibility = Visibility.Visible;
            GridListaPomiarow.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Pokazanie formularza tworzenia/edycji pacjenta
        /// </summary>
        private void ShowDanePacjentaForm() {
            RowDanePacjenta.Height = new GridLength(8, GridUnitType.Star);
            RowDanePacjenta.MinHeight = 250;
            GridDanePacjenta.Visibility = Visibility.Visible;
            GridListaPacjentow.Visibility = Visibility.Collapsed;
            GridListaPomiarow.Visibility = Visibility.Collapsed;
            var row = RowDanePacjenta;
        }

        /// <summary>
        /// Wyczyszczenie pól na formularzu z danymi pacjenta
        /// </summary>
        private void ResetDanePacjentaForm() {
            TextBoxImie.Text = "";
            TextBoxNazwisko.Text = "";
            TextBoxPesel.Text = "";
            TextBoxWzrost.Text = "";
            TextBoxWaga.Text = "";
            ComboBoxPlec.SelectedItem = null;
            TextBoxNotatka.Text = "";
            TextBoxPacjentId.Text = "";
            TextBlockDataUrForm.Text = "";
            TextBlockWiekForm.Text = "";
        }

        /// <summary>
        /// Wypełnienie formularza tworzenia/edycji pacjenta danymi
        /// </summary>
        /// <param name="pd">Dane pacjenta</param>
        private void FillDanePacjentaForm(PatientData pd) {
            TextBoxImie.Text = pd.Imie;
            TextBoxNazwisko.Text = pd.Nazwisko;
            TextBoxPesel.Text = pd.Pesel;
            TextBoxWzrost.Text = pd.Wzrost.ToString();
            TextBoxWaga.Text = pd.Waga.ToString();

            ComboBoxPlec.SelectedValue = pd.Plec;
            var items = ComboBoxPlec.Items;

            TextBoxPacjentId.Text = pd.Id.ToString();

            TextBoxNotatka.Text = pd.Notatka;
        }

        /// <summary>
        /// Utworzenie danych pacjenta na podstawie wartości w formularzu
        /// </summary>
        /// <returns>Dane pacjenta</returns>
        private PatientData CreatePatientData() {
            try {

                (int age, DateTime birth) = CalcBirthFromPesel(TextBoxPesel.Text);

                return new PatientData() {
                    Imie = TextBoxImie.Text,
                    Nazwisko = TextBoxNazwisko.Text,
                    Pesel = TextBoxPesel.Text,
                    Wzrost = double.Parse(TextBoxWzrost.Text),
                    Waga = double.Parse(TextBoxWaga.Text),
                    Plec = ComboBoxPlec.SelectedValue as string,
                    Id = (string.IsNullOrEmpty(TextBoxPacjentId.Text) ? 0 : int.Parse(TextBoxPacjentId.Text)),
                    Notatka = TextBoxNotatka.Text,
                    Wiek = age,
                    DataUrodzenia = birth
                };
            }
            catch(Exception ex) {
                // log + komunikat
                _log.Error(ex);
                return null;
            }
        }

        /// <summary>
        /// Odświeżenie listy pacjentów
        /// </summary>
        /// <param name="search">Tekst do wyszukania (w imieniu lub nazwisku)</param>
        private void RefreshPatientList(string search = "") {
            ListViewPacjenci.ItemsSource = string.IsNullOrEmpty(search) ? repo.GetAll() : repo.GetAll().Where(x => x.NazwiskoImie.ToLower().Contains(search));
        }

        /// <summary>
        /// Wyczyszczenie pola szukajki
        /// </summary>
        private void ClearTextSearch() {
            TextBoxWyszukaj.Text = "";
        }

        /// <summary>
        /// Wypełnienie danych pacjenta na zakładce 'Pomiary'. Jeśli dane mają być wyczyszczone trzeba przesłać w argumencie null.
        /// </summary>
        /// <param name="pd">Dane pacjenta</param>
        private void FillDanePacjentaPomiary(PatientData pd) {
            LabelPacjentImie.Content = pd?.Imie;
            LabelPacjentNazwisko.Content = pd?.Nazwisko;
            LabelPacjentPesel.Content = pd?.Pesel;
            LabelPacjentPlec.Content = pd?.Plec;
            LabelPacjentWaga.Content = pd?.Waga.ToString();
            LabelPacjentWzrost.Content = pd?.Wzrost.ToString();
        }
        
        /// <summary>
        /// Wyznaczenie daty urodzenia i wieku na podstawie peselu
        /// </summary>
        /// <param name="pesel"></param>
        /// <returns></returns>
        private (int age, DateTime birth) CalcBirthFromPesel(string pesel) {

            if (string.IsNullOrEmpty(pesel) || pesel.Length != 11 || !double.TryParse(pesel, out double a))
                return (-1, DateTime.MinValue);


            int.TryParse(pesel.Substring(0, 2), out int year);
            int.TryParse(pesel.Substring(2, 2), out int month);
            int.TryParse(pesel.Substring(4, 2), out int day);

            if (month > 80) {
                year += 1800;
                month -= 80;
            }
            else if (month > 20) {
                year += 2000;
                month -= 20;
            }
            else {
                year += 1900;
            }
            DateTime birth = new DateTime(year, month, day);
            int age = (DateTime.Now.Year - birth.Year - 1) + ((birth.DayOfYear <= DateTime.Now.DayOfYear) ? 1 : 0);

            return (age, birth);
        }


        #endregion

        /// <summary>
        /// Wyświetlenie panelu tutoriala
        /// </summary>
        private void OpenNewSessionTutorial() {
            TabItemPomiar.IsEnabled = true;
            TabControlMain.SelectedItem = TabItemPomiar;

            GridPomiarShowRow(0);
            ComboBoxWybierzProtokol.ItemsSource = GlobalVariables.ProtokolRepository.GetAll();

        }

        /// <summary>
        /// Pokazywanie określonego panelu na zakładce 'Pomiary'
        /// </summary>
        /// <param name="rowNum">0 - szczegółowy tutorial, 1 - uproszczony tutorial, 2 - wykres</param>
        private void GridPomiarShowRow(int rowNum) {
            GridPomiarMain.RowDefinitions[0].Height = new GridLength(0);
            GridPomiarTutorialDetailed.Visibility = Visibility.Collapsed;

            GridPomiarMain.RowDefinitions[1].Height = new GridLength(0);
            GridPomiarTutorialSimple.Visibility = Visibility.Collapsed;

            GridPomiarMain.RowDefinitions[2].Height = new GridLength(0);
            GridPomiarDane.Visibility = Visibility.Collapsed;
            GridPomiarWykres.Visibility = Visibility.Collapsed;

            switch (rowNum) {
                case 0:
                    GridPomiarMain.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Star);
                    GridPomiarTutorialDetailed.Visibility = Visibility.Visible;
                    break;
                case 1:
                    GridPomiarMain.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star);
                    GridPomiarTutorialSimple.Visibility = Visibility.Visible;
                    break;
                case 2:
                default:
                    GridPomiarMain.RowDefinitions[2].Height = new GridLength(1, GridUnitType.Star);
                    GridPomiarDane.Visibility = Visibility.Visible;
                    GridPomiarWykres.Visibility = Visibility.Visible;
                    break;
            }
        }

        /// <summary>
        /// Przejście do panelu z wykresem z panelu wyboru partii mięśni
        /// </summary>
        private void PrzejdzDoPomiaru() {

            // pasuje to zrobić uniwersalnie - niezależnie z jakiego okienka, ma się zachować tak samo
            GridPomiarShowRow(2);
            ComboBoxProtokol.Items.Clear();
            ComboBoxProtokol.Items.Add(GlobalVariables.Session.Protokol);
            ComboBoxProtokol.SelectedItem = GlobalVariables.Session.Protokol;

            ComboBoxPartieMiesni.Items.Clear();
            ComboBoxPartieMiesni.Items.Add(GlobalVariables.Session.ActPartiaMiesni);
            ComboBoxPartieMiesni.SelectedItem = GlobalVariables.Session.ActPartiaMiesni;

            // czyszczenie wykresu etc.
        }

        #region Testy odbierania danych

        private void Button_Click(object sender, RoutedEventArgs e) {
            Logic.LogicManager l = Logic.LogicManager.Instance;
            l.InitSerialPort();
            l.InitReceive();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e) {
            Logic.LogicManager.Instance.EndReceive = true;
            Thread.Sleep(200);
            Logic.LogicManager.Instance.AbortReceiver();
        }

        #endregion

        /// <summary>
        /// Wybranie protokołu na panelu Tutoriala
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxWybierzProtokol_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var combo = sender as ComboBox;
            if(combo != null) {
                var sel = (int)combo.SelectedValue;
                using ( var db = new LiteDB.LiteDatabase("MyDb.db")) {
                    var partie = db.GetCollection<PartiaMiesni>("PartiaMiesni").Find(x => x.ProtokolId == sel).ToList();
                    ComboBoxWybierzMiesnie.ItemsSource = partie;
                }
            }
        }

        /// <summary>
        /// Gotowość pacjenta - czyli naciągnięcie tej nieszczęsnej linki
        /// Trzeba zapytać Marka, co on tam robił - jakaś kalibracja, czy ki chuj
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonGotowoscPacjenta_Click(object sender, RoutedEventArgs e) {
            ButtonRozpocznijPomiar.IsEnabled = true;
        }

        /// <summary>
        /// Rozpoczęcie pomiaru - czyli wklepywanie danych na wykres (w uproszczeniu)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonRozpocznijPomiar_Click(object sender, RoutedEventArgs e) {
            // inicjalizacja
            GlobalVariables.TensometerManager.TensometerActions += GlobalVariables.Session.InsertPoint;
            // funkcja insertująca na wykres
            GlobalVariables.TensometerManager.TensometerActions += GlobalVariables.ChartManager.AddPointToMainLine;

            GlobalVariables.ChartManager.Init();

            ButtonZatrzymajPomiar.IsEnabled = true;
            ButtonPrzerwijPomiar.IsEnabled = true;
        }

        /// <summary>
        /// Zatrzymanie pomiaru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonZatrzymajPomiar_Click(object sender, RoutedEventArgs e) {
            GlobalVariables.TensometerManager.TensometerActions -= GlobalVariables.Session.InsertPoint;
            GlobalVariables.TensometerManager.TensometerActions -= GlobalVariables.ChartManager.AddPointToMainLine;
            GlobalVariables.ChartManager.StopRefreshing();
        }

        /// <summary>
        /// Przerwanie pomiaru - powinno działać nieco inaczej
        /// Na mój rozum - powinno wyzerować to co się stało do tej pory w ramach pomiaru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPrzerwijPomiar_Click(object sender, RoutedEventArgs e) {
            GlobalVariables.TensometerManager.TensometerActions -= GlobalVariables.Session.InsertPoint;
            GlobalVariables.TensometerManager.TensometerActions -= GlobalVariables.ChartManager.AddPointToMainLine;
            GlobalVariables.ChartManager.StopRefreshing();

            // + jeszcze kilka rzeczy
        }

        /// <summary>
        /// Przejście do pomiaru dla kolejnej partii mięśni
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPrzejdzDalej_Click(object sender, RoutedEventArgs e) {
            GridPomiarShowRow(1);
            GlobalVariables.ChartManager.Restart();

            bool comp = GlobalVariables.Session.CompletePartiaMiesni();
            if (comp && GlobalVariables.Session.Completed) {
                // zakończono sesję pomiarową - pokazałbym tu panelTutorial nr 3 z jakimś opisem, że zajebiście itd, i że może zrobić następujące rzeczy
                // na razie puste, do tego momentu w testach na sucho i tak nie dojdę
            }

            TextBlockSesjaPomiarowa.Text = $"Sesja pomiarowa pacjenta: {GlobalVariables.Session.PatientData.NazwiskoImie}";
            TextBlockSeriaPomiarowa.Text = $"Seria pomiarowa nr {GlobalVariables.Session.ActSeriesNum}";

            ListViewUkonczonePartie.ItemsSource = null;
            ComboBoxWybierzMiesnie2.ItemsSource = null;

            ListViewUkonczonePartie.ItemsSource = GlobalVariables.Session.ExaminedPartiaMiesni;
            ComboBoxWybierzMiesnie2.ItemsSource = GlobalVariables.Session.ToExaminePartiaMiesni;

            // załadowanie obrazka - nie można zapomnieć o obrazku

            
        }
    }
}
