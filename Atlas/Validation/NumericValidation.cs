﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Atlas.Validation
{
    public class NumericValidation : ValidationRule {
        public Type ValidationType { get; set; }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            string strValue = Convert.ToString(value);

            if (string.IsNullOrEmpty(strValue))
                return new ValidationResult(false, $"Wprowadź poprawną wartość.");
            bool canConvert = false;
            switch (ValidationType.Name) {

                //case "Boolean":
                //    bool boolVal = false;
                //    canConvert = bool.TryParse(strValue, out boolVal);
                //    return canConvert ? new ValidationResult(true, null) : new ValidationResult(false, $"Input should be type of boolean");
                case "Int32":
                    int intVal = 0;
                    canConvert = int.TryParse(strValue, out intVal);
                    return canConvert ? new ValidationResult(true, null) : new ValidationResult(false, $"Wprowadzona wartość nie jest liczbą.");
                case "Double":
                    double doubleVal = 0;
                    canConvert = double.TryParse(strValue, out doubleVal) && doubleVal > 0;
                    return canConvert ? new ValidationResult(true, null) : new ValidationResult(false, $"Wprowadzona wartość nie jest poprawna.");
                case "Int64":
                    long longVal = 0;
                    canConvert = long.TryParse(strValue, out longVal);
                    return canConvert ? new ValidationResult(true, null) : new ValidationResult(false, $"Wprowadzona wartość nie jest liczbą.");
                default:
                    throw new InvalidCastException($"{ValidationType.Name} is not supported");
            }
        }
       
    }
}
