﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Atlas.Validation
{
    public class NameSurnameValidation : ValidationRule {
        /// <summary>
        /// Jeśli nazwisko - 'N', jeśli imię - 'I'
        /// </summary>
        public string Type { get; set; }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            try {
                string strVal = value as string;

                if (string.IsNullOrEmpty(strVal)) {
                    return new ValidationResult(false, "Wprowadź wartość.");
                }
                else {
                    Regex reg;
                    bool valid;
                    if (Type == "I") {
                        reg = new Regex(@"^([A-ZĄĆĘŁŃŚŻŹ])[a-ząśżźćęółń]+$");
                        valid = reg.IsMatch(strVal);
                        return new ValidationResult(valid, valid ? null : "Imię wygląda na niepoprawne.");
                    }
                    else {
                        reg = new Regex(@"^(?:[a-ząśżźćęółń]+(?:\s*-\s*)?[a-ząśżźćęółń]+)$", RegexOptions.IgnoreCase);
                        valid = reg.IsMatch(strVal);
                        return new ValidationResult(valid, valid ? null : "Nazwisko wygląda na niepoprawne.");
                    }

                }
            }
            catch(Exception ex) {
                // log
                return new ValidationResult(false, $"Wprowadź poprawne {(Type == "I" ? "imię" : "nazwisko")}.");
            }

        }
    }
}
