﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;

namespace Atlas.Validation {

    public class PeselValidationRule : ValidationRule {
        // public Type ValidationType { get; set; }
        private static readonly ILog _log = LogManager.GetLogger("");
        
        private int[] multipliers = new int[] {
            1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1
        };
        
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {

            bool valid = false;

            string strVal = Convert.ToString(value);
            if (string.IsNullOrEmpty(strVal)) {
                return new ValidationResult(valid, "Wprowadzona wartość musi być ciągiem 11 cyfr.");
            }
            else if(strVal.Length != 11) {
                return new ValidationResult(valid, "Niepoprawna długość numeru PESEL. Poprawny numer PESEL ma 11 cyfr.");
            }
            else{
                try {
                    valid = double.TryParse(strVal, out double a);
                    int sum = 0;
                    List<int> peselDig = new List<int>();
                    for (int i = 0; i < multipliers.Length; i++) {
                        int b = int.Parse(strVal[i].ToString());
                        peselDig.Add(b);
                        sum += multipliers[i] * b;
                    }

                    valid = (sum % 10 == 0) && valid;

                    return valid ? new ValidationResult(valid, null) : new ValidationResult(valid, "Poprawny numer PESEL składa się wyłącznie z cyfr");
                    
                }
                catch(Exception ex) {
                    // log
                    _log.Error(ex);
                    return new ValidationResult(valid, "Poprawny numer PESEL składa się wyłącznie z cyfr");
                }
            }
        }
    }
}
